<?php

/*
 * Copyright (C) 2013 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/*
 * Pour les composants identiques, PCAD les exporte chacun sur une ligne
 * différente.
 * Seul le premier du groupe a sa quantité spécifiée: les autres ont une
 * quantité vide.
 * Cette fonction combine donc toutes les lignes pour un même type de composant
 * en une seule, et en combinant tous les refdes ensembles, séparés par des
 * virgules.
 */
function refdes_combine(&$data, $num, $col_num_to_id, $col_id_to_num)
{
  global $debug;

  $newpart = 0; /* Set to 1 when encountering a new part. */
  $reflist = "";
  $newpartkey = "";

  foreach ($data as $key => $row) {

    for ($c = 0; $c < $num; $c++) {
      if ($col_num_to_id[$c] == DESIGNATOR_COL_NAME) {
        $refdes = $row[$c];
      }

      if ($col_num_to_id[$c] == QTY_COL_NAME) {
        if ($row[$c] == "") {
          $newpart = 0;
        } else {
          $newpart = 1;

          /* Save previous reflist if applicable. */
          if ($reflist != "") {
            if ($debug) {
              echo "  REFLIST = " . $reflist . "\n";
            }

            $data[$newpartkey][$col_id_to_num[DESIGNATOR_COL_NAME]] = $reflist;
          }

          $newpartkey = $key;
          $reflist = "";
        }
      }
    }

    if ($newpart == 1) {
      if ($debug) {
        echo "DEBUG: NEWPART\n";
      }

      $reflist = $refdes;
    } else {
      $reflist = $reflist . "," . $refdes;

      /* Discard row. */
      unset($data[$key]);
    }
  }
}

?>
