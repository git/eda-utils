<?php

/*
 * Copyright (C) 2004 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

define("SYTELINE_DSN", "syteline");
define("SYTELINE_USER", "sa");
define("SYTELINE_PASS", "");

define("COLOR_HEADER",   12);
define("COLOR_EVEN_ROW", 13);
define("COLOR_ODD_ROW",  14);

define("XLS_FORMAT_TEXT",     0);
define("XLS_FORMAT_NUMBER",   1);
define("XLS_FORMAT_MONETARY", 2);

require_once 'Spreadsheet/Excel/Writer.php';

$mode_script = "html";

function debug($string)
{
  global $mode_script;

  if ($mode_script == "html")
    echo '<pre class="debug">' . $string . '</pre>';
  else
    echo $string . LF;
}

function debug_err($string)
{
  global $mode_script;

  if ($mode_script == "html")
    echo '<pre class="debug">' . $string . '</pre>';
  else
    fwrite(STDERR, $string . LF);  
}

function debug_gen($string)
{
  if (DISPLAY_DEBUG_GEN) {
    debug ($string);
  }
}

function debug_sql($string)
{
  if (DISPLAY_DEBUG_SQL) {
    debug($string);
  }
}

/* Détection si le script PHP est appelé par le serveur web ou sur la ligne de commande */
function detection_mode_script()
{
  global $mode_script;

  if (isset($_SERVER['REQUEST_METHOD'])) {
    $mode_script = "html";
  } else {
    $mode_script = "cmdline";
  }
}

function windows_to_unix_path( $path )
{
  // Remplacement de "\" par "/"
  return str_replace( "\\", "/", $path );
}


function remove_spaces_from_filename( $file )
{
  $file = str_replace( " ", "\\ ", $file );

  return $file;
}

function strip_ext($name) 
{ 
  $ext = strrchr($name, '.'); 
  if($ext !== false) 
    { 
      $name = substr($name, 0, -strlen($ext)); 
    } 
  return $name; 
}

function strip_trailing_slash($name) 
{ 

  if (substr($name, -1, 1) == '/') {
    $name = substr($name, 0, -1);
  }

  return $name; 
}

function strip_last_dir($name) 
{
  $name = strip_trailing_slash($name);

  $last_dir = strrchr($name, '/'); 
  if($last_dir !== false) 
    { 
      $name = substr($name, 0, -strlen($last_dir)); 
    }
    
  return $name; 
}

/* Création de couleurs personalisées dans la table des couleurs. */
function xls_set_colors($workbook)
{
  /* Entête. */
  $workbook->setCustomColor(COLOR_HEADER, 255, 153, 00);

  /* Rangée paire. */
  $workbook->setCustomColor(COLOR_EVEN_ROW, 255, 255, 255);

  /* Rangée impaire. */
  $workbook->setCustomColor(COLOR_ODD_ROW, 219, 225, 255);
}

/*
 * Écriture dans une cellule, avec la couleur alternée pour les lignes
 * paires et impaires.
 *
 * $format_texte:
 *   true  = texte
 *   false = nombre
 */
function xls_write_cell_gen($row, $col, $value, $format_texte, $entete, $size, $border, $wrap)
{
  global $workbook,$worksheet;

  $value = mb_convert_encoding($value, "ISO-8859-1", "UTF-8");

  /* Création object format. */
  $format =& $workbook->addFormat();

  /* Grosseur du texte. */
  $format->setSize($size);

  if ($wrap) {
    $format->setTextWrap();
  }

  if ($format_texte == XLS_FORMAT_TEXT) {
    $format->setAlign('left');
    $format->setAlign('top');
  } else if ($format_texte == XLS_FORMAT_MONETARY) {
    $format->setNumFormat('$0.00');
  }

  if ($entete == TRUE) {
    /* Texte en gras. */
    $format->setBold(); 
    $format->setFgColor(COLOR_HEADER);
  } else {
    if ($row % 2) {
      $format->setFgColor(COLOR_ODD_ROW);
    } else {
      $format->setFgColor(COLOR_EVEN_ROW);
    }
  }

  if ($border) {
    if ($entete == TRUE) {
      $format->setTop(1);    /* Bordure du haut. */
    }

    $format->setBottom(1); /* Bordure du haut. */
    $format->setRight(1);  /* Bordure de droite. */
    $format->setLeft(1);   /* Bordure de gauche. */
  }

  if ($format_texte == XLS_FORMAT_TEXT) {
    $worksheet->writeString($row, $col, $value, $format);
  } else {
    $worksheet->write($row, $col, $value, $format);
  }
}

function xls_write_col_header($row, $col, $value)
{
  xls_write_cell_gen($row, $col, $value, XLS_FORMAT_TEXT, true, 12, true, true);
}

function xls_write_cell($row, $col, $value, $format_texte)
{
  xls_write_cell_gen($row, $col, $value, $format_texte, false, 11, true, true);
}

function xls_write_price($row, $col, $value)
{
  xls_write_cell_gen($row, $col, $value, XLS_FORMAT_MONETARY, false, 11, true, true);
}

function get_price($item)
{
  /* Connect to a DSN "syteline" */
  $connect = odbc_connect(SYTELINE_DSN, SYTELINE_USER, SYTELINE_PASS);

  /*
   * unit_cost:
   * lst_u_cost: Last
   * avg_u_cost:
   * cur_u_cost:
   */
  $query = "SELECT item, cur_u_cost FROM dbo.item WHERE item='" . $item . "'";

  /* Perform the query. */
  $result = odbc_exec($connect, $query);

  $price = "";

  /* Fetch the data from the database. */
  while(odbc_fetch_row($result)) {
    $item  = odbc_result($result, 1);
    $price = odbc_result($result, 2);
  }

  /* Close the connection. */
  odbc_close($connect);

  return $price;
}

function get_mpn($item)
{
  /* Connect to a DSN "syteline" */
  $connect = odbc_connect(SYTELINE_DSN, SYTELINE_USER, SYTELINE_PASS);

  /*
   * unit_cost:
   * lst_u_cost: Last
   * avg_u_cost:
   * cur_u_cost:
   */
  $query = "SELECT item, MPN FROM dbo.DAP_MPN WHERE item='" . $item . "'";

  /* Perform the query. */
  $result = odbc_exec($connect, $query);

  $mpn = "";

  /* Fetch the data from the database. */
  while(odbc_fetch_row($result)) {
    $item  = odbc_result($result, 1);
    $mpn = odbc_result($result, 2);
  }

  /* Close the connection. */
  odbc_close($connect);

  return $mpn;
}

function get_manufacturer($item)
{
  /* Connect to a DSN "syteline" */
  $connect = odbc_connect(SYTELINE_DSN, SYTELINE_USER, SYTELINE_PASS);

  /*
   * unit_cost:
   * lst_u_cost: Last
   * avg_u_cost:
   * cur_u_cost:
   */
  $query = "SELECT item, MANUFACTURER FROM dbo.DAP_MPN WHERE item='" . $item . "'";

  /* Perform the query. */
  $result = odbc_exec($connect, $query);

  $manufacturer = "";

  /* Fetch the data from the database. */
  while(odbc_fetch_row($result)) {
    $item  = odbc_result($result, 1);
    $manufacturer = odbc_result($result, 2);
  }

  /* Close the connection. */
  odbc_close($connect);

  return $manufacturer;
}

function xls_configure($workbook, $worksheet)
{
  global $col_id_to_xls_num, $csv_infos;

  /* Configration des couleurs de cellules. */
  xls_set_colors($workbook);

  foreach ($csv_infos as $key => $value) {
    $width = $value['width'];

    /* Ajustement de la largeur de chaque colonne. */
    if ($width) {
      /* S'assure que la colonne a été exportée dans le fichier CSV. */
      if (isset($col_id_to_xls_num[$key])) {
        $worksheet->setColumn($col_id_to_xls_num[$key], $col_id_to_xls_num[$key],
                              $width);
      } else {
        echo "Colonne '$key' manquante.\n";
      }
    }
  }

  $worksheet->setLandscape();
  $worksheet->setPaper(PAPER_US_LETTER);
}

/*
 * value: clé
 * c:     Numéro de colonne dans le fichier CSV.
 * xls_c: Numéro de colonne dans Excel.
 */
function determine_col_index($value, $c, $xls_c)
{
  global $col_id_to_num, $col_num_to_id, $col_id_to_xls_num;

  $col_id_to_num[$value] = $c;
  $col_num_to_id[$c] = $value;
  $col_id_to_xls_num[$value] = $xls_c;
}

/* Conversion colonne numérique à lettre:
 *   0 -> A
 *   1 -> B
 *   ...
 */
function col_to_letter($c)
{
  $c = intval($c + 1); /* c part à 0. */

  if ($c <= 0)
    return '';

  $letter = '';

  while($c != 0) {
    $p = ($c - 1) % 26;
    $c = intval(($c - $p) / 26);
    $letter = chr(65 + $p) . $letter;
  }

  return $letter;
}

function bom_import_source($filename)
{
  global $col_id_to_num, $num, $row_num, $add_cost, $csv_infos, $entete, $data;

  $handle = fopen($filename, "r");

  if ($handle == FALSE) {
    echo "Error opening file: " . $filename . "\n";
    exit(1);
  }

  /* Read and import BOM source file. */
  while (($row = fgetcsv($handle, 1024, ',')) !== false) {
    $num = count($row);

    if ($num < 2) {
      /* Ligne vide. */
      continue;
    }

    if ($row_num == 1) {
      /* Entête. */

      if ($add_cost == true) {
        /* Ajout colonne temporaire pour le total cost. */
        $row[$num] = "TotalCost";
        $num++;
      }

      /* Ajout colonne temporaire selon le type de composant. Cela est
       * nécessaire pour faire le tri correctement. */
      $row[$num] = "Type";
      $num++;

      /* Index de la prochaine colonne disponible pour affichage. */
      $k = 0;

      for ($c = 0; $c < $num; $c++) {
        $id = $row[$c];

        determine_col_index($id, $c, $k);

        if (isset($csv_infos[$id])) {
          if ($csv_infos[$id]['width'] != 0) {
            $nom = $csv_infos[$id]['nom'];

            $entete[$k] = $nom;

            $k++;
          }
        } else {
          echo "Colonne '$id' non supportée.\n";
        }
      }
    } else {
      $ref_prefix2 = substr($row[$col_id_to_num[DESIGNATOR_COL_NAME]], 0, 2);
      $company_pn = substr($row[$col_id_to_num[COMPANY_PN_COL_NAME]], 0, 1); /* Pour les vieux BOMs qui n'ont pas
                                                                              * l'attribut NOPOP. */
      if (($ref_prefix2 == "MH") ||
          ($ref_prefix2 == "TP") ||
          ($ref_prefix2 == "FI") ||
          (array_key_exists(ASSEMBLY_COL_NAME, $col_id_to_num) && ($row[$col_id_to_num[ASSEMBLY_COL_NAME]] == DO_NOT_POPULATE_KEYWORD)) ||
          ($company_pn == "*")) {
        /* Enlève les composants non désirés. */
        continue;
      }

      $ref_prefix = substr($ref_prefix2, 0, 1);
      $row[$col_id_to_num['Type']] = $ref_prefix;
      $data[] = $row;
    }

    $row_num++;
  }

  fclose($handle);
}

?>
