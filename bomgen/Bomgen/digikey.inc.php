<?php

/*
 * Copyright (C) 2013 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

define("CUSTOMER_REFERENCE_COL_NAME", COMPANY_PN_COL_NAME);

function import_inventory($filename)
{
  global $debug;

  $data = array();

  $handle = fopen($filename, "r");

  if ($handle == false) {
    echo "Cannot open file: " . $filename . "\n";
    exit(1);
  }

  while (($row = fgetcsv($handle, 1024, ',')) !== false) {
    $num = count($row);

    if ($num < 2) {
      /* Ligne vide. */
      continue;
    }

    $data[] = $row;
  }

  fclose($handle);

  return $data;
}

/*
 * If a part is found in inventory file, exclude it from BOM.
 *
 * If the customer P/N is empty, try to match by manufacturer P/N instead.
 */
function find_part_in_inventory($inventory, $bom_customer_pn, $bom_manufacturer_pn)
{
  foreach ($inventory as $key => $row) {
    $inv_customer_pn = $row[0];

    if ($bom_customer_pn !== "") {
      $match = $bom_customer_pn;
    } else if ($bom_manufacturer_pn !== "") {
      $match = $bom_manufacturer_pn;
    } else {
      echo "Error: Need at least one of customer or manufacturer P/N\n";
      exit(1);
    }

    if ($inv_customer_pn == $match) {
      return true;
    }
  }

  return false;
}

/* Exportation BOM digikey. */
function export_bom_digikey(&$data, $num, $col_num_to_id, $col_id_to_num, $filename, $inventory)
{
  global $debug, $kits;

  $handle = fopen($filename, "w");

  foreach ($data as $key => $row) {
    $num = count($row);

    for ($c = 0; $c < $num; $c++) {
      if (isset($col_num_to_id[$c])) {
        if ($col_num_to_id[$c] == QTY_COL_NAME) {
          $qty = $kits * $row[$c]; /* Multiply unit cost by number of kits to assemble. */
        } else if ($col_num_to_id[$c] == "Part Number") {
          $pn = $row[$c];

          if ($pn == "") {
            $pn = "MISSING";
          }
        } else if ($col_num_to_id[$c] == CUSTOMER_REFERENCE_COL_NAME) {
          $customer_ref = $row[$c];
        } else if ($col_num_to_id[$c] == "Manufacturer") {
          $manuf = $row[$c];
        }

      }
    }

    $exclude_part = find_part_in_inventory($inventory, $customer_ref, $pn);

    if (($pn != "MISSING") && ($exclude_part == false)) {
      $line = $qty . CSV_DK_DELIM . $manuf . CSV_DK_DELIM . $pn . CSV_DK_DELIM . $customer_ref . "\r\n";
      fwrite($handle, $line);
    }
  }

  fclose($handle);
}

?>
