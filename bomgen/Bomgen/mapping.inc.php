<?php

/*
 * Copyright (C) 2013 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/* Common fields for all BOM types. */
define("ASSEMBLY_COL_NAME", "Asm");
define("DO_NOT_POPULATE_KEYWORD", "DNP");
define("COMPANY_PN_COL_NAME", "P/N_LSI");

/* width = 0 -> Ne pas afficher la colonne. */
$csv_infos =
  array(QTY_COL_NAME     => array("nom"   => "Qty",
                                  "width" => 5),
        DESIGNATOR_COL_NAME => array("nom"   => "Designator",
                                  "width" => 30),
        "Description"    => array("nom"   => "Description",
                                  "width" => 45),
        "Value"          => array("nom"   => "Value",
                                  "width" => 15),
        "Voltage"        => array("nom"   => "Voltage",
                                  "width" => 10),
        "Current"        => array("nom"   => "Current",
                                  "width" => 10),
        "Power"          => array("nom"   => "Power",
                                  "width" => 10),
        "Manufacturer"   => array("nom"   => "Manufacturer",
                                  "width" => 30),
        "Part Number"    => array("nom"   => "Manuf. P/N",
                                  "width" => 30),
        COMPANY_PN_COL_NAME => array("nom"   => "LSI P/N",
                                  "width" => 15),
        FOOTPRINT_COL_NAME => array("nom"   => "Footprint",
                                    "width" => 20),
        ASSEMBLY_COL_NAME => array("nom"   => "Assembly",
                                  "width" => 0),
        "UnitCost"       => array("nom"   => "Unit Cost",
                                  "width" => 7),
        "TotalCost"      => array("nom"   => "Total Cost", /* Colonne ajoutée par ce script. */
                                  "width" => 8),
        "Type"           => array("nom"   => "Type", /* Colonne ajoutée par ce script. */
                                  "width" => 0),
        );

?>
