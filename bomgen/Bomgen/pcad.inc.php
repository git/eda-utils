<?php

/*
 * Copyright (C) 2013 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/* PCAD BOM fields definitions. */

define("QTY_COL_NAME", "Count");
define("DESIGNATOR_COL_NAME", "RefDes");
define("FOOTPRINT_COL_NAME", "PatternName");

?>
