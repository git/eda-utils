#!/bin/bash

# Number of cards to assemble
KITS=10

REMOTE="git.company.com"

BOM_IN=$(find . -name \*.csv)
BOM_OUT=${BOM_IN%.csv}.xls
BOM_OUT_TXT=${BOM_IN%.csv}.txt

INV=inventory.csv

REMOTE_DIR=.

OPTS="-t altium -r B -p BRD0056"

if [ ! -f ${BOM_IN} ]; then
    echo "Missing file: ${BOM_IN}"
    exit 1
fi

# Send input BOM:
echo "put ${BOM_IN}" | sftp ${REMOTE}:${REMOTE_DIR}

if [ -f ${INV} ]; then
    # Send inventory file:
    echo "put ${INV}" | sftp ${REMOTE}:${REMOTE_DIR}
    INV_OPTS="-i ${REMOTE_DIR}/${INV}"
fi

# Run remote command to generate Excel BOM:
echo "bomgen.php ${OPTS} ${REMOTE_DIR}/${BOM_IN}" | ssh ${REMOTE}

# Run remote command to generate Digikey BOM:
echo "bomgen.php -k -n ${KITS} ${OPTS} ${INV_OPTS} ${REMOTE_DIR}/${BOM_IN}" | \
    ssh ${REMOTE}

# Fetch Excel output BOM:
echo "get ${BOM_OUT}" | sftp ${REMOTE}:${REMOTE_DIR}

# Fetch Digikey output BOM:
echo "get ${BOM_OUT_TXT}" | sftp ${REMOTE}:${REMOTE_DIR}

