// Redimensionnement du texte des r�f�rences de tous les composants

Procedure PCB_ResizeRefDes_All;
Var
    Board     : IPCB_Board;
    Comp      : IPCB_Component;
    Iterator  : IPCB_BoardIterator;

Begin
    Pcbserver.PreProcess;

    Board := PCBServer.GetCurrentPCBBoard;
    If Not Assigned(Board) Then
    Begin
        ShowMessage('The Current Document is not a Protel PCB Document.');
        Exit;
    End;
    
    // Setup Board iterator
    Iterator        := Board.BoardIterator_Create; 
    Iterator.AddFilter_ObjectSet(MkSet(eComponentObject)); 
    Iterator.AddFilter_LayerSet(AllLayers); 
    Iterator.AddFilter_Method(eProcessAll);
    
    Comp := Iterator.FirstPCBObject;
    While (Comp <> Nil) Do
    Begin
        // On ne modifie pas le num�ro du PCB, qui est la r�f�rence du logo de compagnie
        If CompareString(Comp.Pattern, 'LOGOCOMPANY', 7) Then
        Begin
           Comp := Iterator.NextPCBObject;
           Continue;
        End;

        If CompareString(UpperCase(Comp.Pattern), 'MIRE', 4) Then
        Begin
           Comp := Iterator.NextPCBObject;
           Continue;
        End;

        If CompareString(UpperCase(Comp.Pattern), 'CRACK', 5) Then
        Begin
           Comp := Iterator.NextPCBObject;
           Continue;
        End;

        // On ne modifie pas les fiducials
        If CompareString(UpperCase(Comp.Pattern), 'FIDG', 4) Then
        Begin
           Comp := Iterator.NextPCBObject;
           Continue;
        End;

        // On ne modifie pas la l�gende des couches
        If CompareString(UpperCase(Comp.Pattern), 'LAYERS', 6) Then
        Begin
           Comp := Iterator.NextPCBObject;
           Continue;
        End;
        
        // Call resize function
        ResizeRefDes(Comp);
        
        Comp := Iterator.NextPCBObject;
    End;
    
    Board.BoardIterator_Destroy(Iterator);
    
    Pcbserver.PostProcess;
    Client.SendMessage('PCB:Zoom', 'Action=Redraw', 255, Client.CurrentView);
End;
