// Alignement des attributs des composants dans un sch�ma

// TODO:
//   -Mettre attribut "Mirrored" � FALSE pour les R, C et L

Const
   DEFAULT_FONT_NAME = 'Arial';
   DEFAULT_FONT_SIZE = 10;
   
   R_SYMBOL_LENGTH_MILS     =  400; // Longueur du symbole
   R_SYMBOL_HEIGHT_MILS     =  050; // Hauteur du symbole
   
   C_DISPLAY_VOLTAGE        = True; // Affichage de l'attribut voltage
   C_SYMBOL_LENGTH_MILS     =  300; // Longueur du symbole
   C_SYMBOL_HEIGHT_MILS     =  160; // Hauteur du symbole
   
   L_SYMBOL_LENGTH_MILS     =  500; // Longueur du symbole
   L_SYMBOL_HEIGHT_MILS     =  090; // Hauteur du symbole (2 diff�rentes hauteurs, 50 et 100mil...)
   
   D_SYMBOL_LENGTH_MILS     =  400; // Longueur du symbole
   D_SYMBOL_HEIGHT_MILS     =  150; // Hauteur du symbole (2 diff�rentes hauteurs, 50 et 100mil...)

   PARAM_OFFSET_MILS        =  025; // Distance entre le graphique et le texte
   PARAM_TEXT_HEIGHT_ADJUST =  015; // Pour compenser la hauteur du texte si alignement vertical au centre.
   PARAMS_Y_SEP             =  100; // Distance verticale entre 2 param�tres
   
   TEXT_HEIGHT              =  50; // TEST....
      
   // global variables
Var
   FontID      : TFontID;
   REFDES_POS  : Integer;
   VALUE_POS   : Integer;
   VOLTAGE_POS : Integer;
   POWER_POS   : Integer;
   CURRENT_POS : Integer;
   MPN_POS     : Integer;
   SymbolLengthMils : Integer;
   SymbolHeightMils : Integer;
   value_found : boolean;
   value_par  : ISch_Parameter;
   par_count  : Integer; // Nom,bre de param�tres � afficher
   
Function SCH_AnalyseComponent(Comp : ISch_Component);
Var
   s : String;
      
Begin
    SymbolLengthMils := 0;
    SymbolHeightMils := 0;

    s := Copy(UpperCase(Comp.Designator.Text), 1, 1);
    
    REFDES_POS := 0;
    
   If s = 'R' Then Begin
      SymbolLengthMils := R_SYMBOL_LENGTH_MILS;
      SymbolHeightMils := R_SYMBOL_HEIGHT_MILS;
      VALUE_POS   := 1;
      POWER_POS   := 2;
      par_count   := 3;
   End;
   If s = 'L' Then Begin
      SymbolLengthMils := L_SYMBOL_LENGTH_MILS;
      SymbolHeightMils := L_SYMBOL_HEIGHT_MILS;
      VALUE_POS   := 1;
      CURRENT_POS := 2;
      par_count   := 3;
   End;
   If s = 'C' Then Begin
      SymbolLengthMils := C_SYMBOL_LENGTH_MILS;
      SymbolHeightMils := C_SYMBOL_HEIGHT_MILS;
      VALUE_POS   := 1;
      VOLTAGE_POS := 2;
      par_count   := 3;
   End;
   If s = 'D' Then Begin
      SymbolLengthMils := D_SYMBOL_LENGTH_MILS;
      SymbolHeightMils := D_SYMBOL_HEIGHT_MILS;
      VALUE_POS   := 1; // For LEDs, VALUE = COLOR 
      VOLTAGE_POS := 2;
      CURRENT_POS := 3;
      POWER_POS   := 3;
      par_count   := 3;
   End;
   
   // Pour avoir les ajustements � partir du milieu du composant
   SymbolLengthMils := SymbolLengthMils / 2;
   SymbolHeightMils := SymbolHeightMils / 2;
End;

{***************************************************************************
 * function UpdateSchParmValueByRef()
 *  Perform a schematic component parameter update given a reference to the parameter,
 *  plus it's new value.
 ***************************************************************************}
Function UpdateSchParmValueByRef(component : ISch_Component;
                                 Parameter : ISch_Parameter;
                                 new_value : TDynamicString);
Begin
   { Notify the server process that we're about to make a change. }
   SchServer.RobotManager.SendMessage(Parameter.I_ObjectAddress, c_BroadCast,
                                      SCHM_beginModify, c_NoEventData);

   { Make the change. }
   Parameter.Text := new_value;

   { Notify the server process that we're done making changes. }
   SchServer.RobotManager.SendMessage(Parameter.I_ObjectAddress, c_BroadCast,
                                      SCHM_endModify, c_NoEventData);
End;

Function SCH_ProcessRefDes(Comp : ISch_Component; Parameter : ISch_Parameter);
Var
   CompLoc        : TLocation; // Position du composant
   ParamLoc       : TLocation;
   DX, DY         : Integer;   // Position du param�tre (delta par rapport au composant)
   ParamOffset    : Integer;
   ParamJust      : TTextJustification;
   s          : String;
Begin
   DX := 0;
   DY := 0;
   
   s := Copy(UpperCase(Comp.Designator.Text), 1, 1);
   
   // Trouve le X de d�part, en Y le milieu du composant
   Case Comp.Orientation of
      eRotate0   :
         Begin
            DX := SymbolLengthMils;
         End;
      eRotate90  :
         Begin
            DX := SymbolHeightMils + PARAM_OFFSET_MILS;
            DY := SymbolLengthMils;
         End;
      eRotate180 :
         Begin
            DX := - SymbolLengthMils;
         End;
      eRotate270 :
         Begin
            DX := SymbolHeightMils + PARAM_OFFSET_MILS;
            DY := - SymbolLengthMils;
         End;
   End;
   
   Case Comp.Orientation of
      eRotate0,  eRotate180 :
         Begin
            ParamJust := eJustify_Center;
         end;
      eRotate90, eRotate270 :
         Begin
            ParamJust := eJustify_CenterLeft;
            DY := DY + ((par_count - 1) * (PARAMS_Y_SEP / 2));
         End;
   End;
   
   // Debug
   //If Parameter = Nil Then
   //   ShowMessage('LOC (1) = ' + IntToStr(DX) + ',' + IntToStr(DY));

   If Parameter = Nil Then // Designator
   Begin
      ParamOffset := REFDES_POS;
   End
   Else
   Begin
       ParamOffset := -1;
   
       // Process Parameter, not designator
       If CompareString(UpperCase(Parameter.Name), 'VAL', 3) Then
       Begin
           Parameter.IsHidden := False;
           ParamOffset := VALUE_POS;
       End;
       
       If CompareString(UpperCase(Parameter.Name), 'CAPACITANCE', 11) Then
       Begin
          If s = 'C' Then
          Begin
             Parameter.IsHidden := True;
             If value_found = False Then
             Begin
                UpdateSchParmValueByRef(Comp, value_par, Parameter.Text);
             End;
          End;
       End;
       
       If CompareString(UpperCase(Parameter.Name), 'RESISTANCE (OHMS)', 17) Then
       Begin
          If s = 'R' Then
          Begin
             Parameter.IsHidden := True;
             If value_found = False Then
             Begin
                UpdateSchParmValueByRef(Comp, value_par, Parameter.Text);
             End;
          End;
       End;
       
       If CompareString(UpperCase(Parameter.Name), 'RESISTANCE', 10) Then
       Begin
          If s = 'R' Then
          Begin
             Parameter.IsHidden := True;
          End;
       End;

       If CompareString(UpperCase(Parameter.Name), 'VOLTAGE', 7) Then
       Begin
          If s = 'C' Then
          Begin
             Parameter.IsHidden := False; // force display of voltage parameter
             Parameter.Text := StringReplace(Parameter.Text, ' Volts', 'V', MkSet(rfReplaceAll, rfIgnoreCase));
          End;
          ParamOffset := VOLTAGE_POS;
       End;
              
       If CompareString(UpperCase(Parameter.Name), 'POWER', 5) Then
       Begin
           ParamOffset := POWER_POS;
       End;

       If CompareString(UpperCase(Parameter.Name), 'CURRENT', 7) Then
       Begin
           ParamOffset := CURRENT_POS;
       End;

       If CompareString(UpperCase(Parameter.Name), 'PN', 2) Then
       Begin
           ParamOffset := MPN_POS;
       End;

       If CompareString(UpperCase(Parameter.Name), 'NIA', 3) Then
       Begin
           Parameter.IsHidden := True;
       End;
       
       If ParamOffset < 0 Then
       Begin
           ParamOffset := 2; // TEMP!!!!!!!
       End;
   End;
   
   //if Parameter <> Nil Then
   //   if Parameter.IsHidden = False Then
   //      ShowMessage('Par ' + Parameter.Name + ' = ' + IntToStr(DX) + ',' + IntToStr(DY) + '  OFFSET = ' + IntToStr(ParamOffset));

   Case Comp.Orientation of
      eRotate0,  eRotate180 :
         Begin
            DY := - (SymbolHeightMils + PARAM_OFFSET_MILS + PARAM_TEXT_HEIGHT_ADJUST + (PARAMS_Y_SEP * ParamOffset));
         end;
      eRotate90, eRotate270 :
         Begin
            DY := DY - (PARAMS_Y_SEP * ParamOffset);
         End;
   End;
   
   CompLoc := Comp.GetState_Location;
   ParamLoc := CompLoc;
   
   ParamLoc.X := ParamLoc.X + MilsToCoord(DX);
   ParamLoc.Y := ParamLoc.Y + MilsToCoord(DY);
   
   If Parameter = Nil Then
   Begin
      // Designator
      Comp.Designator.Justification := ParamJust;
      Comp.Designator.SetState_Location(ParamLoc);
      Comp.Designator.Orientation := eRotate0;
   End
   Else
   Begin
      Parameter.Justification := ParamJust;
      Parameter.SetState_Location(ParamLoc);
      Parameter.SetState_Orientation(eRotate0);
   End;
End;

// Standardisation de la police de caract�re     
Function SCH_SetFont(Comp : ISch_Component; Parameter : ISch_Parameter);
Var
   s                : String;
Begin
   If Parameter = Nil Then
      Comp.Designator.FontId := FontID // Designator
   Else
      Parameter.FontId := FontID;
End;

// Check if VALUE parameter exists
Function SCH_check_value(Comp : ISch_Component) : Boolean;
Var
   found      : Boolean;
   PIterator  : ISch_Iterator;
   Parameter  : ISch_Parameter;
Begin
   Try
      found := False;
      PIterator := Comp.SchIterator_Create;
      PIterator.AddFilter_ObjectSet(MkSet(eParameter));

      Parameter := PIterator.FirstSchObject;
      While Parameter <> Nil Do
      Begin
         If CompareString(UpperCase(Parameter.Name), 'VAL', 3) Then
            found := True;
         Parameter := PIterator.NextSchObject;
      End;
   Finally
      Comp.SchIterator_Destroy(PIterator);
      
      if found = False Then
      Begin
         // Add the VALUE parameter to the component.
         value_par          := SchServer.SchObjectFactory(eParameter, eCreate_Default);
         value_par.Name     := 'VAL';
         value_par.ShowName := False;
         value_par.Text     := 'UNDEFINED';
         value_par.IsHidden := True;

         // Place the new parm
         Comp.AddSchObject(value_par);
         SchServer.RobotManager.SendMessage(Comp.I_ObjectAddress, c_BroadCast, SCHM_PrimitiveRegistration, value_par.I_ObjectAddress);
      End;
            
      Result := found;
   End;
End;

Function SCH_AlignRefDes(Comp : ISch_Component);
Var
   PIterator  : ISch_Iterator;
   Parameter  : ISch_Parameter;
Begin
    FontID := SchServer.FontManager.GetFontID(DEFAULT_FONT_SIZE,0,False,False,False,False, DEFAULT_FONT_NAME);
    value_found := SCH_check_value(Comp);
    
    Try
        VALUE_POS   := -1;
        VOLTAGE_POS := -1;
        POWER_POS   := -1;
        CURRENT_POS := -1;
        MPN_POS     := -1;

        SCH_SetFont(Comp, Nil);
        SCH_AnalyseComponent(Comp);

        If SymbolLengthMils <> 0 Then
            SCH_ProcessRefDes(Comp, Nil);
                          
        PIterator := Comp.SchIterator_Create;
        PIterator.AddFilter_ObjectSet(MkSet(eParameter));
            
        Parameter := PIterator.FirstSchObject;
        While Parameter <> Nil Do
        Begin
           If CompareString(UpperCase(Parameter.Name), 'COMMENT', 7) Then
           Begin
              // Pour ne pas afficher le commentaire (#compagnie)
              Parameter.IsHidden := True;
           End
           Else
              If SymbolLengthMils <> 0 Then
                  SCH_ProcessRefDes(Comp, Parameter);
                  
              SCH_SetFont(Comp, Parameter);
              Parameter := PIterator.NextSchObject;
        End;
    Finally
        Comp.SchIterator_Destroy(PIterator);
    End;
End;
