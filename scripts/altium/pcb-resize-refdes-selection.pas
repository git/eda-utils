// Summary: Change reference designator text size of the selected component.

Procedure PCB_ResizeRefDes_Selection;
Var
    Board     : IPCB_Board;
    Comp      : IPCB_Component;
    x,y,      : TCoord;
    
Begin
    Pcbserver.PreProcess;

    Try
        Board := PCBServer.GetCurrentPCBBoard;
        If Not Assigned(Board) Then
        Begin
            ShowMessage('The Current Document is not a Protel PCB Document.');
            Exit;
        End;
    
        Board.ChooseLocation(x,y, 'Choose Component');
        Comp := Board.GetObjectAtXYAskUserIfAmbiguous(x,y,MkSet(eComponentObject),AllLayers, eEditAction_Select);
        If Not Assigned(Comp) Then Exit;
        
        // Call resize function
        ResizeRefDes(Comp);

    Finally
        Pcbserver.PostProcess;
        Client.SendMessage('PCB:Zoom', 'Action=Redraw', 255, Client.CurrentView);
    End;
    
   
End;
