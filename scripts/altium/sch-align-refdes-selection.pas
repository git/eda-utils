// Alignement des attributs du composant s�lectionn� dans un sch�ma
  
Procedure SCH_AlignRefDes_Selection;
Var
    CurrentSheet : ISch_Document;
    Comp :         ISch_Component;
    Iterator :     ISch_Iterator;
              
Begin
    // Check if schematic server exists or not.
    If SchServer = Nil Then Exit;
  
    CurrentSheet := SchServer.GetCurrentSchDocument;
    If CurrentSheet = Nil Then
    Begin
        ShowMessage('The current document is not a schematic document.');
        Exit;
    End;
  
    Try
        SchServer.ProcessControl.PreProcess(CurrentSheet, '');
            
        // Set up iterator to look for Component objects only
        Iterator := CurrentSheet.SchIterator_Create;
        If Iterator = Nil Then Exit;
  
        Iterator.AddFilter_ObjectSet(MkSet(eSchComponent));
        Try
            Comp := Iterator.FirstSchObject;
            While Comp <> Nil Do
            Begin
                // Alignement seulement si le composant a �t� s�lectionn�.
                If Comp.Selection Then SCH_AlignRefDes(Comp);
                Comp := Iterator.NextSchObject;
            End;
  
        Finally
            Currentsheet.SchIterator_Destroy(iterator);
        End;
  
      Finally
        SchServer.ProcessControl.PostProcess(CurrentSheet, '');
    End;

    CurrentSheet.GraphicallyInvalidate;
End;
