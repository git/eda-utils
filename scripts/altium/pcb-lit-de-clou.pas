// Cr�ation d�un dessin pour lit de clou. Efface toutes les r�f�rences
// sauf pour les JP et MP

Const
    MY_REF_SIZE_MILS  = 30;
    MY_REF_WIDTH_MILS = 2;
    
    // La grosseur du texte en fonction de la taille du composant, jusqu�� cette limite.
    TEXT_SIZE_MAX_MILS = 100;
     
    // Ratio de la longueur du texte du refdes par rapport � la longueur maximale permise par les dimensions du composant.
    RATIO_MAXLEN_REFDES = 0.85;
    
    // Ajustement empirique.
    AJUSTEMENT_CENTRAGE = 0.90;
   
Procedure PCB_CreationDessinLitDeClou;
Var
    Board     : IPCB_Board;
    Comp      : IPCB_Component;
    Iterator  : IPCB_BoardIterator;

    strlen,   : Integer;
    StrWidth  : TCoord;
    StrHeight : TCoord;
    OffsetX :   TCoord;
    OffsetY :   TCoord;
    MidX, MidY : Integer;
    MaxLen     :     Integer;
    TempSize : Integer;
    
    Valide   : Boolean;
Begin
    Pcbserver.PreProcess;

    Board := PCBServer.GetCurrentPCBBoard;
    If Not Assigned(Board) Then
    Begin
        ShowMessage('The Current Document is not a Protel PCB Document.');
        Exit;
    End;
        
    // Setup Board iterator 
    Iterator        := Board.BoardIterator_Create; 
    Iterator.AddFilter_ObjectSet(MkSet(eComponentObject)); 
    //Iterator.AddFilter_LayerSet(MkSet(eTopLayer));
    Iterator.AddFilter_LayerSet(AllLayers); 
    Iterator.AddFilter_Method(eProcessAll);
    
    Comp := Iterator.FirstPCBObject;
    While (Comp <> Nil) Do
    Begin
        // Check if Component Name property exists before extracting the text
        If Comp.Name = Nil Then Exit;

        /////ShowMessage('Processing component ' + Comp.Name.Text);

        // Modify the component
        PCBServer.SendMessageToRobots(Comp.I_ObjectAddress, c_Broadcast, PCBM_BeginModify , c_NoEventData);

        Valide := False;

        // On n�affiche pas la r�f�rence...
        Comp.NameOn := False;
        
        // ..sauf pour ces composants:
        If CompareString(Comp.Name.Text, 'MP', 2) Then
            Valide := True;
        If CompareString(Comp.Name.Text, 'JPA', 3) Then
            Valide := True;
        If CompareString(Comp.Name.Text, 'JPB', 3) Then
            Valide := True;
        If CompareString(Comp.Name.Text, 'JPD', 3) Then
            Valide := True;
        If CompareString(Comp.Name.Text, 'JPE', 3) Then
            Valide := True;
        If CompareString(Comp.Name.Text, 'JPF', 3) Then
            Valide := True;
        If CompareString(Comp.Name.Text, 'JPG', 3) Then
            Valide := True;
        If CompareString(Comp.Name.Text, 'JPH', 3) Then
            Valide := True;
        If CompareString(Comp.Name.Text, 'JPI', 3) Then
            Valide := True;
        If CompareString(Comp.Name.Text, 'JPJ', 3) Then
            Valide := True;
        If CompareString(Comp.Name.Text, 'JPK', 3) Then
            Valide := True;
        If CompareString(Comp.Name.Text, 'JPL', 3) Then
            Valide := True;
        If CompareString(Comp.Name.Text, 'JPM', 3) Then
            Valide := True;
        If CompareString(Comp.Name.Text, 'JPN', 3) Then
            Valide := True;
        If CompareString(Comp.Name.Text, 'JPP', 3) Then
            Valide := True;

                    
        If NOT Valide Then
        Begin
            Board.RemovePCBObject(Comp);
            Comp := Iterator.NextPCBObject;
            Continue;
        End
        Else
        Begin
            Comp.NameOn := True;
        End;

        // Et on �limine les commentaires
        Comp.CommentOn := False;

        // On fait une rotation de la r�f�rence selon l�orientation du composant
        // sauf pour les MP qui sont ronds
        If CompareString(Comp.Name.Text, 'MP', 2) Then
        Begin
            Comp.Name.Rotation := 0;
        End
        Else
        Begin
            Case Comp.Rotation of
                0    : Comp.Name.Rotation := 0;
                180  : Comp.Name.Rotation := 0;
                360  : Comp.Name.Rotation := 0;
                90   : Comp.Name.Rotation := 90;
                270  : Comp.Name.Rotation := 90;
            End;
            
            If Comp.Layer = eBottomLayer Then
            Begin
                Case Comp.Name.Rotation of
                    90   : Comp.Name.Rotation := 270;
                End;
            End;
        End;

        FetchComponentMidPoints(Comp, MidX, MidY, MaxLen);
        
        // string length * character_width <= MaxLen
        // character_width = 2/3 * Size en mil.
        //   Ex: Si le size est de 30 mils, la largeur est environ de 20 mils.
        // -> Size = MaxLen / strlen * 3/2 
        strlen := Length(Comp.Name.Text);

        // Adjust size
        Comp.Name.UseTTFonts := False; 
        Comp.Name.Italic := False; 
        Comp.Name.Bold := False; 
        Comp.Name.FontName := 'Default';
        
        TempSize := CoordToMils(MaxLen * RATIO_MAXLEN_REFDES) / strlen * 0.9;
        if TempSize > TEXT_SIZE_MAX_MILS Then
            TempSize := TEXT_SIZE_MAX_MILS;
        
        Comp.Name.Size  := MilsToCoord(TempSize);
        Comp.Name.Width := MilsToCoord(MY_REF_WIDTH_MILS);

        StrWidth := strlen * Comp.Name.Size;
        StrHeight:= Comp.Name.Size;

        If Comp.Layer = eTopLayer Then
        Begin
            Case Comp.Name.Rotation of
                0   :
                    Begin
                        OffsetX := -StrWidth  / 2;
                        OffsetY := -StrHeight / 2;
                    End;
                90  :
                    Begin
                        OffsetX := StrHeight / 2;
                        OffsetY := -StrWidth  / 2;
                    End;
            End;
        End;
        If Comp.Layer = eBottomLayer Then
        Begin
            Case Comp.Name.Rotation of
                0  :
                    Begin
                        OffsetX := StrWidth  / 2;
                        OffsetY := -StrHeight / 2;
                    End;
                270 :
                    Begin
                        OffsetX := -StrHeight / 2;
                        OffsetY := -StrWidth  / 2;
                    End;
            End;
        End;
        
        // Ajustement empirique...    
        Case Comp.Name.Rotation of
            0   : OffsetX := OffsetX * AJUSTEMENT_CENTRAGE;
            90  : OffsetY := OffsetY * AJUSTEMENT_CENTRAGE;
            270 : OffsetY := OffsetY * AJUSTEMENT_CENTRAGE;
        End;

        Comp.Name.XLocation := MidX + OffsetX;
        Comp.Name.YLocation := MidY + OffsetY;

        PCBServer.SendMessageToRobots(Comp.I_ObjectAddress, c_Broadcast, PCBM_EndModify , c_NoEventData);
        //Client.SendMessage('PCB:Zoom', 'Action=Redraw', 255, Client.CurrentView);

        Comp := Iterator.NextPCBObject;
    End;
    
    Board.BoardIterator_Destroy(Iterator);
    
    Pcbserver.PostProcess;
    Client.SendMessage('PCB:Zoom', 'Action=Redraw', 255, Client.CurrentView);
End;
