// Standardisation du nom des paramètres des composants d'une librairie

Procedure CleanLibrary;
Var
   CurrentLib      : ISch_Lib;
   LibraryIterator : ISch_Iterator;
   LibComp         : ISch_Component;
   PIterator       : ISch_Iterator;
   Parameter       : ISch_Parameter;
   
   RefPrefix         : String;
   ImplIterator      : ISch_Iterator;
   SchImplementation : ISch_Implementation;
   
Begin
   If SchServer = Nil Then Exit;
   CurrentLib := SchServer.GetCurrentSchDocument;
   If CurrentLib = Nil Then Exit;

   // check if the document is a schematic library
   If CurrentLib.ObjectID <> eSchLib Then
   Begin
      ShowError('Please open schematic library.');
      Exit;
   End;

   // create a library iterator to look for
   // symbols in the currently focussed library.
   LibraryIterator := CurrentLib.SchLibIterator_Create;
   LibraryIterator.AddFilter_ObjectSet(MkSet(eSchComponent));
   Try
      // obtain the first symbol in the library
      LibComp := LibraryIterator.FirstSchObject;
      While LibComp <> Nil Do
      Begin
         
         RefPrefix := Copy(UpperCase(LibComp.Designator.Text), 1, 1);
         
         // Corrections champs description
         If UpperCase(LibComp.ComponentDescription) = 'RESISTOR FIXE' Then
            LibComp.ComponentDescription := 'Resistor';
         If UpperCase(LibComp.ComponentDescription) = 'CAPACITOR CERAMIQUE COG' Then
            LibComp.ComponentDescription := 'Capacitor Ceramic C0G';
         If UpperCase(LibComp.ComponentDescription) = 'CAPACITOR CERAMIQUE NPO' Then
            LibComp.ComponentDescription := 'Capacitor Ceramic NP0';
         If UpperCase(LibComp.ComponentDescription) = 'CAPACITOR CERAMIQUE X5R' Then
            LibComp.ComponentDescription := 'Capacitor Ceramic X5R';
         If UpperCase(LibComp.ComponentDescription) = 'CAPACITOR CERAMIQUE X7R' Then
            LibComp.ComponentDescription := 'Capacitor Ceramic X7R';
         If UpperCase(LibComp.ComponentDescription) = 'CAPACITOR CERAMIQUE S-SERIE' Then
            LibComp.ComponentDescription := 'Capacitor Ceramic NP0';

         // look for parameters associated with this symbol in a library.
         Try
            PIterator := LibComp.SchIterator_Create;
            PIterator.AddFilter_ObjectSet(MkSet(eParameter));

            Parameter := PIterator.FirstSchObject;
            While Parameter <> Nil Do
            Begin
               If CompareString(UpperCase(Parameter.Name), 'MANUFACTURIER', 13) Then
                  Parameter.Name := 'Manufacturer';
               
               If CompareString(UpperCase(Parameter.Name), 'MANUFACTURER', 12) Then
               Begin
                  Parameter.Text := UpperCase(Parameter.Text); // Conversion majuscules
                  
                  If CompareString(Parameter.Text, 'CAL CHIP', 8) Or
                        CompareString(Parameter.Text, 'CAL-CHIP', 8) Or
                        CompareString(Parameter.Text, 'CAL_CHIP', 8) Or
                        CompareString(Parameter.Text, 'CALCHIP', 7) Then
                     Parameter.Text := 'CAL-CHIP';
                  
                  If CompareString(Parameter.Text, 'ON SEMI', 7) Or
                        CompareString(Parameter.Text, 'ONSEMI', 6) Then
                     Parameter.Text := 'ON SEMICONDUCTOR';
                  
                  If CompareString(Parameter.Text, 'TEXAS', 5) Or
                        CompareString(Parameter.Text, 'TI', 2) Then
                     Parameter.Text := 'TEXAS INSTRUMENTS';
                  
                  If CompareString(Parameter.Text, 'ANALOG DEVICE', 13) Then
                     Parameter.Text := 'ANALOG DEVICES';

                  If CompareString(Parameter.Text, 'VISHAY', 6) Then
                     Parameter.Text := 'VISHAY';
               End;
               
               If CompareString(UpperCase(Parameter.Name), 'PUISSANCE', 9) Then
                  Parameter.Name := 'Power';
               
               If CompareString(UpperCase(Parameter.Name), 'NO MANUF', 8) Then
                  Parameter.Name := 'MPN';
               
               If CompareString(UpperCase(Parameter.Name), 'TOL', 3) Then
                  Parameter.Name := 'Tolerance';
               
               ImplIterator := LibComp.SchIterator_Create;
               ImplIterator.AddFilter_ObjectSet(MkSet(eImplementation));
               SchImplementation := ImplIterator.FirstSchObject;
               While SchImplementation <> Nil Do
               Begin
                  If StringsEqual(SchImplementation.ModelType, 'PCBLIB') Then
                  Begin
                     If CompareString(UpperCase(SchImplementation.ModelName), '1206-016', 8) Then
                        SchImplementation.ModelName := RefPrefix + '_1206_016';
                     
                     If CompareString(UpperCase(SchImplementation.ModelName), '0402', 4) Then
                        SchImplementation.ModelName := RefPrefix + '_0402';
                     If CompareString(UpperCase(SchImplementation.ModelName), '0603', 4) Then
                        SchImplementation.ModelName := RefPrefix + '_0603';
                     If CompareString(UpperCase(SchImplementation.ModelName), '0805', 4) Then
                        SchImplementation.ModelName := RefPrefix + '_0805';
                     If CompareString(UpperCase(SchImplementation.ModelName), '1206', 4) Then
                        SchImplementation.ModelName := RefPrefix + '_1206';
                     If CompareString(UpperCase(SchImplementation.ModelName), '1210', 4) Then
                        SchImplementation.ModelName := RefPrefix + '_1210';
                     If CompareString(UpperCase(SchImplementation.ModelName), '1812', 4) Then
                        SchImplementation.ModelName := RefPrefix + '_1812';
                     If CompareString(UpperCase(SchImplementation.ModelName), '2010', 4) Then
                        SchImplementation.ModelName := RefPrefix + '_2010';
                     If CompareString(UpperCase(SchImplementation.ModelName), '2225', 4) Then
                        SchImplementation.ModelName := RefPrefix + '_2225';
                     
                     // Remove duplicates
                     If CompareString(UpperCase(SchImplementation.ModelName), RefPrefix + '_0402', 6) And Not SchImplementation.IsCurrent Then
                        LibComp.RemoveSchObject(SchImplementation);
                     
                  End;
                  SchImplementation := ImplIterator.NextSchObject;
               End;
               LibComp.SchIterator_Destroy(ImplIterator);

               Parameter := PIterator.NextSchObject;
            End;
         Finally
            LibComp.SchIterator_Destroy(PIterator);
         End;
         // obtain the next symbol in the library
         LibComp := LibraryIterator.NextSchObject;
      End;
   Finally
      // done with looking for symbols and parameters.
      // destroy the library iterator.
      CurrentLib.SchIterator_Destroy(LibraryIterator);
   End;

End;
